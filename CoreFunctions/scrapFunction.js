const axios = require('axios');
const cheerio = require('cheerio');


module.exports = {
    async pagerequest(httplink) {
        const response = await axios.get(httplink);
        let httpcode = await response.status;
        let listData = [];
        try {
            if(httpcode === 200){
                let $ = await cheerio.load(response.data);
                let reactClassMH = await $('div[data-react-class=ModuleHeader]').get();
                let parsed = JSON.parse(reactClassMH[0].attribs['data-react-props']);
                await listData.push(parsed.content);
                let reactClassMB = await $('div[data-react-class=ModuleBrick]').get();
                let treactClassMB = await $('div[data-react-class=ModuleBrick]').length;
                for(let i = 0; i< treactClassMB; i++){
                    let parsed = await JSON.parse(reactClassMB[i].attribs['data-react-props']);
                    await listData.push(parsed.content);
                }
            }else {
                console.log(httpcode);
            }
        }catch (e) {
            console.log(e);
        }
        return listData
    }
};