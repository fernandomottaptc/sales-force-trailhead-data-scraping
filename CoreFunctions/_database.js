const pg = require('pg');

const client = new pg.Client({
    user: "",
    host: "",
    database: "",
    password: "",
    port: "5432",
    ssl: true
});
module.exports = client;
