const sfq1 = require('./SfQuery/SfQuery_Trail.js');
const scf = require('./CoreFunctions/scrapFunction.js');
const dbinsertTB = require('./CoreFunctions/trail_badge_Insert.js');
const dbconn = require('./CoreFunctions/_database.js');
const dbSelectTH = require('./SfQuery/SfLinkQuery');
const dropTB = require('./SfQuery/SfDropTB');


async function main() {
    await dbconn.connect();
    await dropTB.dropTables(dbconn);
    let thlink = await dbSelectTH.trailLink(dbconn);
    //console.log(thlink[0].rows.length);
    //console.log(thlink[0].rows[0].endpoint__c);

    //db.insert('Fernando', 'Leite');
    //console.log(sfdata);
    //await console.log(data.length);
    //let data = await scf.pagerequest(sfdata.dados[0].Endpoint__c);
    // for(let i = 0; i < data.length; i++){
    //     await apilist.push(data[i].apiName);
    // }
    // await db.insert(apilist);

    // console.log(data);
    // console.log(data[0].apiName);

    // await console.log(await sfdata.dados[0].Endpoint__c);
    // console.log(await data[0].apiName);

    // try {
    //     let data = await scf.pagerequest('https://trailhead.salesforce.com/content/learn/modules/spring-18-release-highlights');
    //     console.log(data);
    // }catch (e) {
    //     console.log(e.request.connection._httpMessage.res.statusCode);
    // }
    //console.log(sfdata.dados.length);

    for(let i = 0; i < thlink[0].rows.length;i++){
        let apilist = [];
        try {
            let data = await scf.pagerequest(thlink[0].rows[i].endpoint__c);
            try {
                for(let b = 0; b < data.length; b++){
                    await apilist.push(data[b].apiName);
                }
                await dbinsertTB.insert(apilist, dbconn);
            }catch (e) {
                console.log(e);
            }
        }catch (e) {
            console.log(e.request.connection._httpMessage.res.statusCode);
        }
    }
    console.log('Processo Concluido com SUCESSO.');
    await dbconn.end();
}

main();

