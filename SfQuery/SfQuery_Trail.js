const sf = require('./salesForceConnector.js');
let callBack;

module.exports = {
    async getSfData() {
        let conexao = sf.sfConnection();
        await conexao.login(
            "",
            "",
            function(err, userInfo) {
                if (err) { return console.error(err); }
                // console.log("User ID: " + userInfo.id);
                // console.log("Org ID: " + userInfo.);

            });
        await conexao.query(
            "SELECT Id, Endpoint__c, Points__c " +
                  "FROM trailheadapp__Trail__c " +
                  "WHERE Points__c != null OR Points__c = null",
            function (err, result) {
                if (err) { return console.error(err); }
                //console.log("total : " + result.totalSize);
                //console.log("fetched : " + result.records.length);
                callBack = {total: result.totalSize, dados: result.records}
            });
        await conexao.logout(function(err) {
            if (err) { return console.error(err); }
        });
        return callBack;
    }
};

