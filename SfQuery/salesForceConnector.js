const jsforce = require('jsforce');


module.exports = {
    sfConnection(){
        let conn = new jsforce.Connection({
            oauth2: {
                loginUrl: "https://login.salesforce.com",
                client_id: "",
                client_secret: ""
            },
            instanceUrl: 'https://ever-walk-dev-ed.lightning.force.com/',
            accessToken: "",
            refresh_token: ""
        });
        return conn;
    }
};
