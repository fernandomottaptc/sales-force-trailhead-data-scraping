module.exports = {
    async dropTables(db) {
        await db.query('drop table dbb9j8c8ohuh3e.everwalk.trail_badge');
        await console.log('Tabela removida');
        await db.query('create table ' +
            'everwalk.trail_badge(id serial not null constraint trail_badge_pk primary key, trail text, badge text);');
        await db.query('alter table everwalk.trail_badge owner to stespliftuqgar');
        await db.query('create unique index trail_badge_id_uindex on everwalk.trail_badge (id)');
        await console.log('Tabela (trail_badge) Criada com SUCESSO.');

    }
};



